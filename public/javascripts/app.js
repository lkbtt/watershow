const CONSTANT = {
    toc: {
        key: 'toc',
        title: '总有机碳',
        unit: 'PPM',
        minValue: 0,
        maxValue: 5,
        defaultValue: 0,
        dot: 1,
        description: '总有机碳是指水体中溶解性和悬浮性有机物含碳的总量，主要包含石油、医药、有机农药、多氯联苯、苯酚、塑料残留等，对人体有致癌危险，反映水中有机污染的严重程度。中国饮用水标准为小于5ppm，美国标准为小于1ppm。',
        imageText: ['有机农药', '石油污染', '医药残留']
    },
    tds: {
        key: 'tds',
        title: '溶解固体',
        unit: 'PPM',
        minValue: 0,
        maxValue: 1000,
        defaultValue: 0,
        dot: 0,
        description: '总溶解固体是指水里全部溶质的总量，以TDS表示。包含矿物质和金属离子等物质，影响水质的软硬程度，是衡量水质的参考指标。中国标准为小于500ppm。',
        imageText: ['溶解的矿物质', '其他杂质', '盐']
    },
    turbidity: {
        key: 'turbidity',
        title: '浊度',
        unit: 'NTU',
        minValue: 0,
        maxValue: 6,
        defaultValue: 0,
        dot: 1,
        description: '浊度指水的浑浊度，是由于不溶性物质如泥沙、铁锈和絮状物的存在而引起液体的透明度降低的一种量度。因为不溶性物质成分较为复杂，常作为一个参考指标来衡量水质。中国标准为小于3NTU。',
        imageText: ['泥沙', '铁锈', '絮状物']
    }
};
const TOP_VALUE = [70, 1750];
let water = CONSTANT['toc'];
const INIT_TIME = 6000;

function init(key) {
    // $("#animation").animate({marginTop: '1920px', opacity: 0}, INIT_TIME * 2);
    water = CONSTANT[key];
    initWebSocket();
    $(".best-value").text('< ' + water.minValue + ' ' + water.unit);
    $(".worst-value").text('> ' + water.maxValue + ' ' + water.unit);
    $(".title").text(water.title);
    $(".water-value-unit").text(water.unit);
    $(".description").text(water.description);
    $(".image-text").eq(0).text(water.imageText[0]);
    $(".image-text").eq(1).text(water.imageText[1]);
    $(".image-text").eq(2).text(water.imageText[2]);

    window.setTimeout(function () {
        $("#image-groups").removeClass('toc').removeClass('tds').removeClass('turbidity').addClass(key);
        // autoChange();

        $('#main').fadeIn(1000);

    }, INIT_TIME - 1000);
    window.setTimeout(function () {
        $("#animation").remove();
    }, INIT_TIME - 500);
    window.setTimeout(function () {
        let valueDiff = water.maxValue - water.minValue;
        let random = Math.random() * valueDiff + water.minValue;
        random = water.dot ? random.toFixed(water.dot) : ~~random;
        autoChangeFun(random);
    }, INIT_TIME);

}

function autoChange() {

    // window.setInterval(function () {
    //     let valueDiff = water.maxValue - water.minValue;
    //     let random = Math.random() * valueDiff + water.minValue;
    //     random = water.dot ? random.toFixed(water.dot) : ~~random;
    //     autoChangeFun(random);
    // }, 3000);
}

function autoChangeFun(value, noAnimation) {
    // 改数值
    $(".water-value-number").text(value);
    // 移动左侧水滴
    let topDiff = TOP_VALUE[1] - TOP_VALUE[0];
    let valueDiff = water.maxValue - water.minValue;
    let top = Math.min(Math.abs(value - water.minValue) / valueDiff * topDiff + TOP_VALUE[0], TOP_VALUE[1]);
    if (noAnimation) {
        $("#water").css({top: top + 'px'});
    } else {
        $("#water").stop().animate({top: top + 'px'}, 1000);
    }
    // 改数值
    // window.setInterval(function () {
    //     window.location.reload();
    // }, 20000);

}

function getUrlParam(name) {
    let reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    let r = window.location.search.substr(1).match(reg);
    if (r != null) return decodeURI(r[2]);
    return null;
}

let webSocket;

function initWebSocket() {
    if ("WebSocket" in window) {
        webSocket = new WebSocket("ws://10.0.4.84:3100");
        webSocket.onopen = function () {
            console.log('连接成功');
        };
        webSocket.onmessage = function (evt) {
            var data = JSON.parse(evt.data);
            console.log(data);
            window.location.reload();
        };
        webSocket.onerror = function (evt) {
            console.log("连接失败");
        };
        webSocket.onclose = function () {
            console.log('连接重试');
            setTimeout(function () {
                initWebSocket();
            }, 10000);
        };
    } else {
        alert("无法兼容您的浏览器暂,请升级更换浏览器再试");
    }
}

function fullScreen() {
    let docElm = document.documentElement;
    if (docElm.requestFullscreen) {
        docElm.requestFullscreen();
    } else if (docElm.msRequestFullscreen) {
        docElm = document.body; //overwrite the element (for IE)
        docElm.msRequestFullscreen();
    } else if (docElm.mozRequestFullScreen) {
        docElm.mozRequestFullScreen();
    } else if (docElm.webkitRequestFullScreen) {
        docElm.webkitRequestFullScreen();
    }
}

function bindEvent() {
    // $('#image-groups').on('click',function () {
    //     fullScreen();
    // })

}
