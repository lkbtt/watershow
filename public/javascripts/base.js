$(document).ready(function () {

    initWebSocket();

});

function initImgRowHeight() {
    var windowHeight = $(window).height();
    var windowWidth = $(window).width();
    if (windowHeight / windowWidth < (3 / 4)) {
        $("body").css({
            "width": windowHeight * 4 / 3+100,
            "padding-right":100
        });
    } else {
        $("body").css({
            "height": windowWidth * 3 / 4,
            "margin-top": Math.max((windowHeight - (windowWidth * 3 / 4)) / 2 - 6, 0)
        });
        windowHeight = windowWidth * 3 / 4;
    }
    $(".media").height(windowHeight - 10);
    var imgHeight = windowHeight * 0.22;
    $(".container  .target").css({
        "height": imgHeight,
        "width": $(".container .row .col-md-3").width(),
    });
    $(".container .machine .target").css({
        "height": $(".machine .ring.score").height(),
        "width": $(".machine .ring.score").width(),
    });
    $(".container  .row.circle").height(imgHeight);
    $(".container  .img img").css({
        "max-height": imgHeight,
        "max-width": imgHeight,
        "padding-bottom": imgHeight * 0.02
    });
    $(".container  .img img.animation").css({
        "max-height": imgHeight * 1,
        "max-width": imgHeight * 1,
        "padding": imgHeight * 0.11
    });
    $(".container .row .col-md-3").height(imgHeight);
    $(".container .row .col-md-9").height(imgHeight);

    $(".container .row .col-md-6:eq(0)").css({
        "height": $(".container .row .col-md-3:eq(0)").height(),
        "margin-top": 0
    });
    $(".container .row .col-md-6 .column_gradient:eq(0)").width($(".container .row .col-md-6:eq(0)").width() * 0.2);
    $(".container .row.machine .media:eq(0)").css("marginTop", -0.65 * windowHeight);
    $(".container .row.machine .media:eq(1)").css("marginTop", -$(".container .row.machine .media:eq(0)").height());
    $(".container .row.machine .media:eq(2)").css("marginTop", -$(".container .row.machine .media:eq(0)").height());
    $(".container .row.machine img.ring.score").css({
        "marginTop": -0.545 * windowHeight,
        "width": windowHeight * 4 / 3 * 0.16,
        "left": "50%",
        "transform": "translate(-65%, 0)",
        "margin-left":-42
    });
    $(".container .row.machine .target.ring").css({
        "marginTop": -0.535 * windowHeight,
        "width": windowHeight * 4 / 3 * 0.16,
        "height": windowHeight * 4 / 3 * 0.16,
        "top": "unset",
        "left": "50%",
        "transform": "translate(-65%, 0)",
        "margin-left": -42
    });
    var tm = windowHeight / 1536;
    $(".target-num").css({
        "font-size": tm * 56
    });
    $(".target-name").css("font-size", tm * 26);
    $(".target-unit").css("font-size", tm * 32);
    $("table.title").css("font-size", tm * 80);
    $("#score.target-num").css({
        "font-size": tm * 72
    });
    $(".button-text").css("font-size", tm * 26);
    $(".button").css({
        "margin-top": -$(".button").height() * 1.5,
        "margin-right": -$(".row:eq(0)").width() * 1 / 18
    });
}


var firstOnResizeFire = true;

function onWindowSizeChanged() {
    window.onresize = function () {
        if (firstOnResizeFire) {
            window.location.reload();
            firstOnResizeFire = false;
            setTimeout(function () {
                firstOnResizeFire = true;
            }, 500);
        }
    }
}


var webSocket;

function initWebSocket() {
    if ("WebSocket" in window) {
        webSocket = new WebSocket("ws://192.168.8.215:3100");
        webSocket.onopen = function () {
        };
        webSocket.onmessage = function (evt) {
            var data = JSON.parse(evt.data);
            startAnimation(currentQuality, data);
        };
        webSocket.onerror = function (evt) {
            console.log("连接失败");
        };
        webSocket.onclose = function () {
            setTimeout(function () {
                initWebSocket();
            }, 10000);
        };
    }
    else {
        alert("无法兼容您的浏览器暂,请升级更换浏览器再试");
    }
}

var ZERO = {toc: 0, tds: 0, cl: 0, ph: 7.0, turbidity: 0, score: 0, isSource: true};
var currentQuality = ZERO;
var isAnimation = false;
var queue = [];

function startAnimation(from, to) {
    if (!isAnimation) {
        isAnimation = true;
        if (ZERO === from || from.isSource != to.isSource) {
            toggleSourceFilterButton(to.isSource);
            startPipingLeftAnimation(function () {
                startTargetCircleAnimation(function () {
                    startPipingRightAnimation(to.score);
                    startQualityAnimation(from, to, function () {
                        isAnimation = false;
                        while (queue.length > 0) {
                            if (queue.length = -1) {
                                startAnimation(to, queue.pop());
                            } else {
                                queue.pop()
                            }
                        }
                    });
                })
            });
        } else {
            startQualityAnimation(from, to, function () {
                isAnimation = false;
                while (queue.length > 0) {
                    if (queue.length = -1) {
                        startAnimation(to, queue.pop());
                    } else {
                        queue.pop()
                    }
                }
            });
        }
        currentQuality = to;
    } else {
        queue.push(to);
    }
}

function startTargetCircleAnimation(callback) {
    $("img.circle.animation").addClass("animate-start");
    setTimeout(function () {
        $("img.circle.animation").removeClass("animate-start");
        callback();
    }, 2200);
}

function toggleSourceFilterButton(isSource) {
    setButtonImg(isSource);
}

function startQualityAnimation(from, to, callback) {
    var tocImgElement = $("img.toc");
    var tdsImgElement = $("img.tds");
    var turbidityImgElement = $("img.turbidity");
    var clImgElement = $("img.cl");
    var scoreImgElement = $("img.score:eq(1)");

    var tocElement = $("#toc");
    var tdsElement = $("#tds");
    var turbidityElement = $("#turbidity");
    var clElement = $("#cl");
    var scoreElement = $("#score");
    var count = 0;
    var tocIncrement = (to.toc - from.toc) / 30;
    var tdsIncrement = (to.tds - from.tds) / 30;
    var turbidityIncrement = (to.turbidity - from.turbidity) / 30;
    var clIncrement = (to.cl - from.cl) / 30;
    var scoreIncrement = (to.score - from.score) / 30;
    setCircleBgByQuality(tocImgElement, to.toc, TOC_GREEN, TOC_YELLOW);
    setCircleBgByQuality(tdsImgElement, to.tds, TDS_GREEN, TDS_YELLOW);
    setCircleBgByQuality(turbidityImgElement, to.turbidity, TURBIDITY_GREEN, TURBIDITY_YELLOW);
    setClCircleBgByQuality(clImgElement, to.cl, CL_GREEN_MIN, CL_GREEN_MAX);
    setScoreCircleBgByQuality(scoreImgElement, to.score);
    var time=100;
    if(from.isSource==to.isSource){
        time=20;
    }
    var animIntervalTask = setInterval(function () {
        count++;
        if (count == 30) {
            clearInterval(animIntervalTask);
            tocElement.html(to.toc.toFixed(1));
            tdsElement.html(to.tds.toFixed(0));
            turbidityElement.html(to.turbidity.toFixed(1));
            clElement.html(to.cl.toFixed(1));
            scoreElement.html(to.score.toFixed(1));
            callback();
        } else {
            var tmpToc = (from.toc + tocIncrement * count).toFixed(1);
            var tmpTds = (from.tds + tdsIncrement * count).toFixed(0);
            var tmpTurbidity = (from.turbidity + turbidityIncrement * count).toFixed(1);
            var tmpCl = (from.cl + clIncrement * count).toFixed(1);
            var tmpScore = (from.score + scoreIncrement * count).toFixed(1);
            tocElement.html(tmpToc);
            tdsElement.html(tmpTds);
            turbidityElement.html(tmpTurbidity);
            clElement.html(tmpCl);
            scoreElement.html(tmpScore);
        }
    }, time);
}

function setCircleBgByQuality(element, num, green, yellow) {
    if (num >= yellow) {
        element.attr("src", "images/circle_red.png")
    } else if (num < green) {
        element.attr("src", "images/circle_green.png")
    } else {
        element.attr("src", "images/circle_yellow.png")
    }
}

function setClCircleBgByQuality(element, num, greenMin, greenMax) {
    if (num >= greenMin && num <= greenMax) {
        element.attr("src", "images/circle_green.png")
    } else {
        element.attr("src", "images/circle_yellow.png")
    }
}

function setPhCircleBgByQuality(element, num) {
    if (num <= PH_YELLOW_MIN || num >= PH_YELLOW_MAX) {
        element.attr("src", "images/circle_red.png")
    } else if (num > PH_GREEN_MIN && num < PH_GREEN_MAX) {
        element.attr("src", "images/circle_green.png")
    } else {
        element.attr("src", "images/circle_yellow.png")
    }
}

function setScoreCircleBgByQuality(element, num) {
    var src;
    if (num > SCORE_GREEN) {
        src = "images/score_green.png";
    } else if (num > SCORE_YELLOW) {
        src = "images/score_yellow.png";
    } else {
        src = "images/score_red.png";
    }
    if (src && src.length > 0 && -1 == element[0].src.lastIndexOf(src)) {
        var count = 10;
        var task1 = setInterval(function () {
            count--;
            if (count < 0) {
                clearInterval(task1);
                element.attr("src", src);
                count = 0;
                var task2 = setInterval(function () {
                    count++;
                    if (count > 20) {
                        clearInterval(task2);
                        element.css("opacity", 1);
                    } else {
                        element.css("opacity", count * 0.05);
                    }
                }, 150);
            } else {
                element.css("opacity", 0.1 * count);
            }
        }, 20);
    }
}

function startPipingLeftAnimation(callback) {
    var imgElement = $("img.piping:eq(0)");
    var count = 0;
    var task = setInterval(function () {
        count++;
        imgElement.attr("src", "images/piping0" + PrefixInteger(count, 2) + ".png");
        if (count == 25) {
            clearInterval(task);
            callback();
        }
    }, 50);
}

function startPipingRightAnimation(num) {
    var srcBegin;
    if (num > SCORE_GREEN) {
        srcBegin = 1;
    } else if (num > SCORE_YELLOW) {
        srcBegin = 3;
    } else {
        srcBegin = 2;
    }
    var imgElement = $("img.piping:eq(1)");
    var count = 0;
    var task = setInterval(function () {
        count++;
        imgElement.attr("src", "images/piping" + srcBegin + PrefixInteger(count, 2) + ".png");
        if (count == 51) {
            $("img.piping:eq(0)").attr("src", "images/piping026.png");
            clearInterval(task);
        }
    }, 30);
}


function setButtonImg(isSource) {
    if (isSource) {
        $(".button .source img").attr("src", "images/button-source-checked.png");
        $(".button .filter img").attr("src", "images/button-filter-unchecked.png");
        $(".button .source .button-text").removeClass("text-white");
        $(".button .source .button-text").addClass("text-green");
        $(".button .filter .button-text").removeClass("text-green");
        $(".button .filter .button-text").addClass("text-white");
    } else {
        $(".button .source img").attr("src", "images/button-source-unchecked.png");
        $(".button .filter img").attr("src", "images/button-filter-checked.png");
        $(".button .source .button-text").removeClass("text-green");
        $(".button .source .button-text").addClass("text-white");
        $(".button .filter .button-text").removeClass("text-white");
        $(".button .filter .button-text").addClass("text-green");
    }
}

function PrefixInteger(num, length) {
    return (Array(length).join('0') + num).slice(-length);
}


//进入全屏
function requestFullScreen() {
    var de = document.documentElement;
    if (de.requestFullscreen) {
        de.requestFullscreen();
    } else if (de.mozRequestFullScreen) {
        de.mozRequestFullScreen();
    } else if (de.webkitRequestFullScreen) {
        de.webkitRequestFullScreen();
    }
}

//退出全屏
function exitFullscreen() {
    var de = document;
    if (de.exitFullscreen) {
        de.exitFullscreen();
    } else if (de.mozCancelFullScreen) {
        de.mozCancelFullScreen();
    } else if (de.webkitCancelFullScreen) {
        de.webkitCancelFullScreen();
    }
}

const TOC_GREEN = 1.5;
const TOC_YELLOW = 2.5;
const TDS_GREEN = 90;
const TDS_YELLOW = 150;
const TURBIDITY_GREEN = 0.5;
const TURBIDITY_YELLOW = 1.5;
const PH_GREEN_MIN = 6.5;
const PH_GREEN_MAX = 7.5;
const PH_YELLOW_MIN = 6;
const PH_YELLOW_MAX = 8;
const CL_GREEN_MIN = 0.1;
const CL_GREEN_MAX = 1;
const SCORE_GREEN = 7;
const SCORE_YELLOW = 5;

